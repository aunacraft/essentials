package net.aunacraft.essentials.listener;

import net.aunacraft.essentials.Essentials;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class DamageListener implements Listener {

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntityType() == EntityType.PLAYER) {
            if (Essentials.getInstance().getGodManager().isPlayerInGodMode(((Player) e.getEntity()))) {
                e.setDamage(0D);
                e.setCancelled(true);
            }
        }
    }

}
