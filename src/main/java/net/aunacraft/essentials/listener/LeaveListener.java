package net.aunacraft.essentials.listener;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.event.impl.PlayerUnregisterEvent;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.essentials.Essentials;

public class LeaveListener {

    public LeaveListener() {
        AunaAPI.getApi().registerEventListener(PlayerUnregisterEvent.class, this::handleUnregister);
    }

    public void handleUnregister(PlayerUnregisterEvent e) {
        AunaPlayer ap = e.getPlayer();

        String lastServer = AunaCloudAPI.getServiceAPI().getLocalService().getDisplayName();
        if (lastServer.startsWith("Lobby")) {
            ap.getPlayerMeta().set("essentials.lastserver", AunaCloudAPI.getServiceAPI().getLocalService().getDisplayName().split("-")[0]);
        } else {
            ap.getPlayerMeta().set("essentials.lastserver", AunaCloudAPI.getServiceAPI().getLocalService().getDisplayName());
        }

        ap.getPlayerMeta().set("essentials.lastgm", ap.toBukkitPlayer().getGameMode().toString());

        if (Essentials.getInstance().getFlyManager().canPlayerFly(ap.toBukkitPlayer())) {
            Essentials.getInstance().getFlyManager().toggleFly(ap.toBukkitPlayer());
        }

        if (Essentials.getInstance().getSessionManager().isPlayerInSession(ap.toBukkitPlayer())) {
            Essentials.getInstance().getSessionManager().getSessionByPlayer(ap.toBukkitPlayer()).left();
        }

        ap.getPlayerMeta().set("essentials.vanish", Essentials.getInstance().getVanishManager().isPlayerInVanish(ap.getUuid()));

        ap.updateMeta();
    }
}
