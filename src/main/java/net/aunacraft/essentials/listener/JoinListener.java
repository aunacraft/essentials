package net.aunacraft.essentials.listener;


import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.event.impl.PlayerRegisterEvent;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.essentials.Essentials;
import net.aunacraft.essentials.systems.vanish.VanishPlayer;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

public class JoinListener {

    public JoinListener() {
        AunaAPI.getApi().registerEventListener(PlayerRegisterEvent.class, this::handlePlayerRegister);
    }

    public void handlePlayerRegister(PlayerRegisterEvent e) {
        AunaPlayer ap = e.getPlayer();
        if (!ap.getPlayerMeta().isNull("essentials.lastserver")) {
            String lastserver = ap.getPlayerMeta().getString("essentials.lastserver");
            String lastgm = ap.getPlayerMeta().getString("essentials.lastgm");
            Player p = ap.toBukkitPlayer();


            if (((lastserver.equalsIgnoreCase("Lobby")) && (AunaCloudAPI.getServiceAPI().getLocalService().getDisplayName().split("-")[0].equalsIgnoreCase(lastserver)))
                    || (AunaCloudAPI.getServiceAPI().getLocalService().getDisplayName().equalsIgnoreCase(lastserver))) {

                //Bukkit is retardet and throws an error if you call gamemode change async
                Bukkit.getServer().getScheduler().runTask(Essentials.getInstance(), () ->
                        p.setGameMode(GameMode.valueOf(lastgm)));

            }
        }

        int vanishRank = ap.getRank().getOrderNumber();
        if (!ap.getPlayerMeta().isNull("essentials.vanish")) {
            boolean isVanish = ap.getPlayerMeta().getBoolean("essentials.vanish");
            if (isVanish) {
                VanishPlayer vp = new VanishPlayer(ap.getUuid(), vanishRank);
                Essentials.getInstance().getVanishManager().addVanishPlayer(vp);
                Bukkit.getOnlinePlayers().forEach(player -> {
                    if (!vp.canThisPlayerBeSeen(AunaAPI.getApi().getPlayer(player.getUniqueId()).getRank().getOrderNumber())) {
                        player.hidePlayer(Essentials.getInstance(), vp.getPlayer());
                    }
                });
            }
        }
        Essentials.getInstance().getVanishManager().getVanishPlayers().forEach(player -> {
            if (!player.canThisPlayerBeSeen(ap.getRank().getOrderNumber())) {
                ap.toBukkitPlayer().hidePlayer(Essentials.getInstance(), player.getPlayer());
            }
        });
    }

}
