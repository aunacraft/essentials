package net.aunacraft.essentials.util;


import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;

public class MessagesUtil {

    public static TextComponent createHoverAbleTextWithCommandExecuted(String text, String hoverText, String clickCommand) {
        TextComponent textComponent = new TextComponent(text);
        textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(hoverText)));
        textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, clickCommand));
        return textComponent;
    }

}
