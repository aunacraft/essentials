package net.aunacraft.essentials.systems.session;

import lombok.Getter;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.essentials.Essentials;
import net.aunacraft.essentials.util.MessagesUtil;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.jetbrains.annotations.NotNull;

@Getter
public class Session {

    private final long startedAt;
    private final int secondsTillTimeOut;
    private final String sessionName;
    private final AunaPlayer sender;
    private AunaPlayer target;
    private SessionState currentState;

    public Session(@NotNull AunaPlayer sender, @NotNull AunaPlayer target, @NotNull String sessionName) {
        this.sender = sender;
        this.target = target;
        this.startedAt = System.currentTimeMillis();
        secondsTillTimeOut = 120;
        this.sessionName = sessionName;
    }

    public void onCheck() {
        // 0 TIMEOUT, 1 = ACCEPTED, 2 = DENIED, 3 = LEFT, 4 = CANCEL, 5 = ERROR
        if (currentState == SessionState.WAITING) {
            if ((startedAt + (secondsTillTimeOut * 1000L)) <= System.currentTimeMillis()) {
                stop(0);
                currentState = SessionState.TIMEOUT;
                return;
            }
            if (!Bukkit.getOnlinePlayers().contains(target.toBukkitPlayer())) {
                left();
                return;
            }
            if (!Bukkit.getOnlinePlayers().contains(sender.toBukkitPlayer())) {
                left();
                return;
            }
        } else if (currentState == SessionState.ACCEPTED) {
            stop(1);
        } else if (currentState == SessionState.DENIED) {
            stop(2);
        } else if (currentState == SessionState.LEFT) {
            stop(3);
        } else if (currentState == SessionState.CANCEL) {
            stop(4);
        } else if (currentState == SessionState.ERROR) {
            stop(5);
        }
    }

    public boolean start() {
        if ((target == null) || (!Bukkit.getOnlinePlayers().contains(target.toBukkitPlayer()))) {
            target = null;
            currentState = SessionState.ERROR;
            stop(0);
            return false;
        }
        if (Bukkit.getOnlinePlayers().contains(sender.toBukkitPlayer())) {
            TextComponent textComponentAccept = MessagesUtil.createHoverAbleTextWithCommandExecuted(
                    target.getMessage("essentials.session.requested.tc.accept"),
                    target.getMessage("essentials.session.requested.tc.accept.hover"), "/tpa accept");
            TextComponent textComponentDeny = MessagesUtil.createHoverAbleTextWithCommandExecuted(
                    target.getMessage("essentials.session.requested.tc.deny"),
                    target.getMessage("essentials.session.requested.tc.deny.hover"), "/tpa deny");
            TextComponent text1 = new TextComponent(target.getMessage("essentials.session.requested.1", sender.getName(), sessionName));
            TextComponent text2 = new TextComponent(target.getMessage("essentials.session.requested.2"));
            TextComponent text3 = new TextComponent(target.getMessage("essentials.session.requested.3"));

            target.toBukkitPlayer().spigot().sendMessage(text1, textComponentAccept, text2, textComponentDeny, text3);
            //target.sendMessage("essentials.session.requested", sender.getName(), sessionName);
            sender.sendMessage("essentials.session.request.sent", target.getName(), sessionName);
            return true;
        } else {
            currentState = SessionState.ERROR;
        }
        return false;
    }

    private void stop(final int reason) {

        // 0 TIMEOUT, 1 = ACCEPTED, 2 = DENIED, 3 = LEFT, 4 = CANCEL, 5 = ERROR
        Essentials.getInstance().getSessionManager().removeSession(this);
        if (reason == 0) {
            target.sendMessage("essentials.session.timeout", sessionName, sender.getName());
            sender.sendMessage("essentials.session.timeout", sessionName, target.getName());

        } else if (reason == 1) {
            target.sendMessage("essentials.session.accepted", sessionName, sender.getName());
            sender.sendMessage("essentials.session.accepted", sessionName, target.getName());
            sender.toBukkitPlayer().teleport(target.toBukkitPlayer().getLocation());

        } else if (reason == 2) {
            target.sendMessage("essentials.session.denied", sessionName, sender.getName());
            sender.sendMessage("essentials.session.denied", sessionName, target.getName());

        } else if (reason == 3) {
            if (Bukkit.getOnlinePlayers().contains(target.toBukkitPlayer())) {
                target.sendMessage("essentials.session.left", sessionName, sender.getName());
            } else if (Bukkit.getOnlinePlayers().contains(sender.toBukkitPlayer())) {
                sender.sendMessage("essentials.session.left", sessionName, target.getName());
            }

        } else if (reason == 4) {
            target.sendMessage("essentials.session.canceled", sessionName, sender.getName());
            sender.sendMessage("essentials.session.canceled", sessionName, target.getName());

        } else if (reason == 5) {
            if (target != null) {
                target.sendMessage("essentials.session.error", sessionName, "target null");
            } else if (sender != null) {
                sender.sendMessage("essentials.session.error", sessionName, "sender null");
            }

        }
    }

    public void deny() {
        currentState = SessionState.DENIED;
    }

    public void cancel() {
        currentState = SessionState.CANCEL;
    }

    public void accept() {
        currentState = SessionState.ACCEPTED;
    }

    public void left() {
        currentState = SessionState.LEFT;
    }

    public enum SessionState {
        WAITING, ACCEPTED, DENIED, LEFT, ERROR, TIMEOUT, CANCEL
    }

}
