package net.aunacraft.essentials.systems.session;

import net.aunacraft.essentials.Essentials;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class SessionManager {

    List<Session> sessions;

    public SessionManager() {
        this.sessions = new ArrayList<>();
    }

    public void startCheckingState(int delayInSeconds) {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Essentials.getInstance(), () -> {
                    List<Session> toCheck = new ArrayList<>(sessions);
                    for (Session session : toCheck) {
                        session.onCheck();
                    }
                }
                , 0, delayInSeconds * 20L);
    }

    public void addSession(@NotNull Session session) {
        sessions.add(session);
    }

    public void removeSession(@NotNull Session session) {
        sessions.remove(session);
    }

    public boolean denySession(@NotNull Player player) {
        Session session = getSessionByPlayer(player);
        if (session == null) {
            return false;
        }
        if (session.getTarget().toBukkitPlayer().getUniqueId().equals(player.getUniqueId())) {
            session.deny();
            return true;
        }
        return false;
    }

    public boolean cancelSession(@NotNull Player player) {
        Session session = getSessionByPlayer(player);
        if (session == null) {
            return false;
        }
        if (session.getSender().toBukkitPlayer().getUniqueId().equals(player.getUniqueId())) {
            session.cancel();
            return true;
        }
        return false;
    }

    public boolean isPlayerInSession(Player player) {
        return getSessionByPlayer(player) != null;
    }

    @Nullable
    public Session getSessionByPlayer(Player player) {
        for (Session session : sessions) {
            if ((session.getSender().toBukkitPlayer().getUniqueId().equals(player.getUniqueId()))
                    || (session.getTarget().toBukkitPlayer().getUniqueId().equals(player.getUniqueId()))) {
                return session;
            }
        }
        return null;
    }

}
