package net.aunacraft.essentials.systems.vanish;


import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Getter
public class VanishPlayer {

    private final UUID uuid;
    private final int vanishRank;

    public VanishPlayer(@NotNull UUID uuid, int vanishRank) {
        this.uuid = uuid;
        this.vanishRank = vanishRank;
    }

    public static VanishPlayer getVanishPlayerFromAunaPlayer(@NotNull AunaPlayer player) {
        return new VanishPlayer(player.getUuid(), Essentials.getInstance().getVanishManager().getVanishRankForRank(player.getRank()));
    }

    public boolean canThisPlayerBeSeen(int otherVanishRank) {
        return otherVanishRank >= this.vanishRank;
    }


    public Player getPlayer() {
        return Bukkit.getPlayer(uuid);
    }

}
