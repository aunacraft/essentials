package net.aunacraft.essentials.systems.vanish;

import lombok.Getter;
import net.aunacraft.api.player.PlayerRank;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
public class VanishManager {

    List<VanishPlayer> vanishPlayers;

    public VanishManager() {
        vanishPlayers = new ArrayList<>();
    }

    public void addVanishPlayer(VanishPlayer player) {
        vanishPlayers.add(player);
    }

    public void removeVanishPlayer(VanishPlayer player) {
        vanishPlayers.remove(player);
    }

    public boolean isPlayerInVanish(UUID uuid) {
        return getVanishPlayer(uuid) != null;
    }

    public boolean canPlayerSeeOtherPlayer(int ownVanishRank, UUID targetUUID) {
        VanishPlayer p = getVanishPlayer(targetUUID);
        return p.canThisPlayerBeSeen(ownVanishRank);
    }

    public VanishPlayer getVanishPlayer(UUID uuid) {
        for(VanishPlayer player : vanishPlayers) {
            if(player.getUuid().equals(uuid)) {
                return player;
            }
        }
        return null;
    }

    public int getVanishRankForRank(PlayerRank rank) {
        return rank.getOrderNumber();
    }

}
