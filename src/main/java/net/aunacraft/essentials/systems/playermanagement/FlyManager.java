package net.aunacraft.essentials.systems.playermanagement;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class FlyManager {

    List<Player> playersInGodMode;

    public FlyManager() {
        playersInGodMode = new ArrayList<>();
    }

    public boolean canPlayerFly(Player player) {
        return playersInGodMode.contains(player);
    }

    public int toggleFly(Player player) {
        if (playersInGodMode.contains(player)) {
            playersInGodMode.remove(player);
            return 2;
        } else {
            playersInGodMode.add(player);
            return 1;
        }
    }

}
