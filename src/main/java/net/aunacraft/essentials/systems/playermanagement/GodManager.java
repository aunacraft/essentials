package net.aunacraft.essentials.systems.playermanagement;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class GodManager {

    List<Player> playersInGodMode;

    public GodManager() {
        playersInGodMode = new ArrayList<>();
    }

    public boolean isPlayerInGodMode(Player player) {
        return playersInGodMode.contains(player);
    }

    public int toggleGodMode(Player player) {
        if (playersInGodMode.contains(player)) {
            playersInGodMode.remove(player);
            return 2;
        } else {
            playersInGodMode.add(player);
            return 1;
        }
    }


}
