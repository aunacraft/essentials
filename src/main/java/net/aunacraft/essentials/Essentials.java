package net.aunacraft.essentials;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.essentials.commands.*;
import net.aunacraft.essentials.listener.DamageListener;
import net.aunacraft.essentials.listener.JoinListener;
import net.aunacraft.essentials.listener.LeaveListener;
import net.aunacraft.essentials.systems.playermanagement.FlyManager;
import net.aunacraft.essentials.systems.playermanagement.GodManager;
import net.aunacraft.essentials.systems.session.SessionManager;
import net.aunacraft.essentials.systems.vanish.VanishManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Essentials extends JavaPlugin {

    private static Essentials instance;
    private SessionManager sessionManager;
    private FlyManager flyManager;
    private GodManager godManager;
    private VanishManager vanishManager;

    public static Essentials getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;

        sessionManager = new SessionManager();
        flyManager = new FlyManager();
        godManager = new GodManager();
        vanishManager = new VanishManager();

        sessionManager.startCheckingState(1);

        AunaAPI.getApi().getMessageService().applyPrefix("essentials", "essentials.prefix");

        registerCommands();
        registerListener();
        super.onEnable();
    }

    void registerListener() {
        new JoinListener();
        new LeaveListener();
        Bukkit.getPluginManager().registerEvents(new DamageListener(), this);
    }

    void registerCommands() {
        AunaAPI.getApi().registerCommand(new DayCommand());
        AunaAPI.getApi().registerCommand(new FeedCommand());
        AunaAPI.getApi().registerCommand(new FlyCommand());
        AunaAPI.getApi().registerCommand(new GamemodeCommand());
        AunaAPI.getApi().registerCommand(new GodCommand());
        AunaAPI.getApi().registerCommand(new HealCommand());
        AunaAPI.getApi().registerCommand(new NightCommand());
        AunaAPI.getApi().registerCommand(new RainCommand());
        AunaAPI.getApi().registerCommand(new SunCommand());
        AunaAPI.getApi().registerCommand(new TpaCommand());
        AunaAPI.getApi().registerCommand(new TpHereCommand());
        AunaAPI.getApi().registerCommand(new VanishCommand());
    }

    public SessionManager getSessionManager() {
        return sessionManager;
    }

    public FlyManager getFlyManager() {
        return flyManager;
    }

    public GodManager getGodManager() {
        return godManager;
    }

    public VanishManager getVanishManager() {
        return vanishManager;
    }
}
