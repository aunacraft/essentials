package net.aunacraft.essentials.commands;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.autocompleetion.impl.PlayerCompletionHandler;
import net.aunacraft.api.commands.builder.CommandBuilder;
import net.aunacraft.api.commands.builder.ParameterBuilder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class FeedCommand implements AunaCommand {

    @Override
    public net.aunacraft.api.commands.Command createCommand() {
        return CommandBuilder.beginCommand("feed")
                .permission("essentials.feed")
                .parameter(ParameterBuilder.beginParameter("target")
                        .permission("essentials.feed.other")
                        .optional()
                        .autoCompletionHandler(new PlayerCompletionHandler())
                        .build())
                .handler((p, context, args) -> {
                    if (context.hasParameter("target")) {
                        Player t = Bukkit.getPlayer(args[0]);
                        if (t != null) {
                            t.setFoodLevel(20);
                            AunaAPI.getApi().getPlayer(t.getUniqueId()).sendMessage("essentials.feed");
                            p.sendMessage("essentials.feed.other", t.getName());
                        } else {
                            p.sendMessage("essentials.feed.targetnull", args[1]);
                        }
                    } else {
                        p.toBukkitPlayer().setFoodLevel(20);
                        AunaAPI.getApi().getMessageService().sendMessage(p, "essentials.feed");
                    }
                }).build();
    }
}
