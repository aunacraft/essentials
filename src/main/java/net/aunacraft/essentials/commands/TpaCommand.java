package net.aunacraft.essentials.commands;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.Command;
import net.aunacraft.api.commands.autocompleetion.impl.PlayerCompletionHandler;
import net.aunacraft.api.commands.builder.CommandBuilder;
import net.aunacraft.api.commands.builder.ParameterBuilder;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.essentials.Essentials;
import net.aunacraft.essentials.systems.session.Session;
import org.bukkit.entity.Player;

public class TpaCommand implements AunaCommand {
    @Override
    public Command createCommand() {
        return CommandBuilder.beginCommand("tpa")
                .subCommand(CommandBuilder.beginCommand("request")
                        .parameter(ParameterBuilder.beginParameter("player")
                                .required()
                                .autoCompletionHandler(new PlayerCompletionHandler())
                                .build())
                        .handler((aunaPlayer, context, strings) -> {
                            if (Essentials.getInstance().getSessionManager().isPlayerInSession(aunaPlayer.toBukkitPlayer())) {
                                aunaPlayer.sendMessage("essentials.session.isinsession",
                                        Essentials.getInstance().getSessionManager().getSessionByPlayer(aunaPlayer.toBukkitPlayer()).getSessionName());
                                return;
                            }
                            AunaPlayer target = AunaAPI.getApi().getPlayer(context.getParameterValue("player", Player.class).getUniqueId());
                            if (Essentials.getInstance().getSessionManager().isPlayerInSession(target.toBukkitPlayer())) {
                                aunaPlayer.sendMessage("essentials.session.isinsession.target", target.getName());
                                return;
                            }
                            Session session = new Session(aunaPlayer, target, "Tpa");
                            if(session.start()) {
                                Essentials.getInstance().getSessionManager().addSession(session);
                            }
                        })
                        .build())
                .subCommand(CommandBuilder.beginCommand("cancel")
                        .handler((aunaPlayer, context, strings) -> {
                            Session session = Essentials.getInstance().getSessionManager().getSessionByPlayer(aunaPlayer.toBukkitPlayer());
                            if (session == null) {
                                aunaPlayer.sendMessage("essentials.session.nosession");
                                return;
                            }
                            if (session.getSender().toBukkitPlayer().getUniqueId().equals(aunaPlayer.toBukkitPlayer().getUniqueId())) {
                                session.cancel();
                            } else {
                                session.deny();
                            }
                        })
                        .build())
                .subCommand(CommandBuilder.beginCommand("deny")
                        .handler((aunaPlayer, context, strings) -> {
                            Session session = Essentials.getInstance().getSessionManager().getSessionByPlayer(aunaPlayer.toBukkitPlayer());
                            if (session == null) {
                                aunaPlayer.sendMessage("essentials.session.nosession");
                                return;
                            }
                            if (session.getTarget().toBukkitPlayer().getUniqueId().equals(aunaPlayer.toBukkitPlayer().getUniqueId())) {
                                session.deny();
                            } else {
                                session.cancel();
                            }
                        })
                        .build())
                .subCommand(CommandBuilder.beginCommand("accept")
                        .handler((aunaPlayer, context, strings) -> {
                            Session session = Essentials.getInstance().getSessionManager().getSessionByPlayer(aunaPlayer.toBukkitPlayer());
                            if (session == null) {
                                aunaPlayer.sendMessage("essentials.session.nosession");
                                return;
                            }
                            if (session.getTarget().toBukkitPlayer().getUniqueId().equals(aunaPlayer.toBukkitPlayer().getUniqueId())) {
                                session.accept();
                            } else {
                                aunaPlayer.sendMessage("essentials.session.accept.nottarget");
                            }
                        })
                        .build())
                .build();
    }
}
