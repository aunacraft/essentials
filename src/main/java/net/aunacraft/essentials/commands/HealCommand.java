package net.aunacraft.essentials.commands;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.Command;
import net.aunacraft.api.commands.autocompleetion.impl.PlayerCompletionHandler;
import net.aunacraft.api.commands.builder.CommandBuilder;
import net.aunacraft.api.commands.builder.ParameterBuilder;
import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;

public class HealCommand implements AunaCommand {

    @Override
    public Command createCommand() {
        return CommandBuilder.beginCommand("heal")
                .permission("essentials.heal")
                .parameter(ParameterBuilder.beginParameter("target")
                        .permission("essentials.heal.other")
                        .optional()
                        .autoCompletionHandler(new PlayerCompletionHandler())
                        .build())
                .handler((p, context, args) -> {
                    if (context.hasParameter("target")) {
                        Player t = Bukkit.getPlayer(context.getParameterValue("target", String.class));
                        if (t != null) {
                            t.setHealth(t.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
                            t.setFoodLevel(20);
                            AunaAPI.getApi().getPlayer(t.getUniqueId()).sendMessage("essentials.heal");
                            p.sendMessage("essentials.heal.other", t.getName());
                        } else {
                            p.sendMessage("essentials.heal.targetnull", context.getParameterValue("target", String.class));
                        }
                    } else {
                        p.toBukkitPlayer().setHealth(p.toBukkitPlayer().getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
                        p.toBukkitPlayer().setFoodLevel(20);
                        p.sendMessage("essentials.heal");
                    }
                }).build();
    }
}
