package net.aunacraft.essentials.commands;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.Command;
import net.aunacraft.api.commands.builder.CommandBuilder;
import org.bukkit.entity.Player;

public class DayCommand implements AunaCommand {
    @Override
    public Command createCommand() {
        return CommandBuilder.beginCommand("day")
                .permission("essentials.day")
                .handler((aunaPlayer, commandContext, strings) -> {
                    Player p = aunaPlayer.toBukkitPlayer();
                    p.getWorld().setTime(0);
                    AunaAPI.getApi().getPlayer(p.getUniqueId()).sendMessage("essentials.day");
                }).build();
    }
}
