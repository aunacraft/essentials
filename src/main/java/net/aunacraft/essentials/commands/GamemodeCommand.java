package net.aunacraft.essentials.commands;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.Command;
import net.aunacraft.api.commands.autocompleetion.impl.PlayerCompletionHandler;
import net.aunacraft.api.commands.builder.CommandBuilder;
import net.aunacraft.api.commands.builder.ParameterBuilder;
import net.aunacraft.essentials.commands.autocompletion.GameModeAutoCompletionHandler;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

public class GamemodeCommand implements AunaCommand {

    @Override
    public Command createCommand() {
        return CommandBuilder.beginCommand("gamemode")
                .permission("essentials.gamemode")
                .aliases("gm")
                .parameter(ParameterBuilder.beginParameter("gm")
                        .required()
                        .autoCompletionHandler(new GameModeAutoCompletionHandler())
                        .build())
                .parameter(ParameterBuilder.beginParameter("target")
                        .optional()
                        .permission("essentials.gamemode.other")
                        .optional()
                        .autoCompletionHandler(new PlayerCompletionHandler())
                        .build())
                .handler((p, context, args) -> {
                    GameMode gameMode = context.getParameterValue("gm", GameMode.class);
                    String gameModeStringIdentifier = gameMode.toString().toLowerCase();
                    if (context.hasParameter("target")) {
                        Player target = context.getParameterValue("target", Player.class);
                        target.setGameMode(gameMode);
                        AunaAPI.getApi().getPlayer(target.getUniqueId()).sendMessage("essentials.gamemode." + gameModeStringIdentifier);
                        p.sendMessage("essentials.gamemode." + gameModeStringIdentifier + ".other", target.getName());
                    } else {
                        p.toBukkitPlayer().setGameMode(gameMode);
                        p.sendMessage("essentials.gamemode." + gameModeStringIdentifier);
                    }
                }).build();
    }
}