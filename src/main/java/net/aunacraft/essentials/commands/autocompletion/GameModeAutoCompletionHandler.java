package net.aunacraft.essentials.commands.autocompletion;

import net.aunacraft.api.commands.autocompleetion.AutoCompletionHandler;
import org.bukkit.GameMode;

import java.util.Arrays;
import java.util.List;

public class GameModeAutoCompletionHandler implements AutoCompletionHandler<GameMode> {
    @Override
    public List<String> getCompletions() {
        return Arrays.asList("0", "survival", "1", "creative", "2", "adventure", "3", "spectator");
    }

    @Override
    public boolean isValid(String in) {
        if(in.equalsIgnoreCase("0") || in.equalsIgnoreCase("survival")) return true;
        else if(in.equalsIgnoreCase("1") || in.equalsIgnoreCase("creative")) return true;
        else if(in.equalsIgnoreCase("2") || in.equalsIgnoreCase("adventure")) return true;
        else return in.equalsIgnoreCase("3") || in.equalsIgnoreCase("spectator");
    }

    @Override
    public GameMode convertString(String in) {
        if(in.equalsIgnoreCase("0") || in.equalsIgnoreCase("survival")) return GameMode.SURVIVAL;
        else if(in.equalsIgnoreCase("1") || in.equalsIgnoreCase("creative")) return GameMode.CREATIVE;
        else if(in.equalsIgnoreCase("2") || in.equalsIgnoreCase("adventure")) return GameMode.ADVENTURE;
        else return GameMode.SPECTATOR;
    }
}
