package net.aunacraft.essentials.commands.autocompletion;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.commands.autocompleetion.AutoCompletionHandler;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class ExtendedPlayerAutoCompletionHandler implements AutoCompletionHandler<String> {
    @Override
    public List<String> getCompletions() {
        List<String> returnValue = new ArrayList<>();
        returnValue.add("all");
        for(Player all : Bukkit.getOnlinePlayers()) {
            returnValue.add(AunaAPI.getApi().getPlayer(all.getUniqueId()).getName());
        }
        return returnValue;
    }

    @Override
    public boolean isValid(String s) {
        return (Bukkit.getPlayer(s) != null) || (s.equalsIgnoreCase("all"));
    }

    @Override
    public String convertString(String s) {
        return s;
    }
}
