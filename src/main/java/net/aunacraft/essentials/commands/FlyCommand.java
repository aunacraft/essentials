package net.aunacraft.essentials.commands;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.Command;
import net.aunacraft.api.commands.autocompleetion.impl.PlayerCompletionHandler;
import net.aunacraft.api.commands.builder.CommandBuilder;
import net.aunacraft.api.commands.builder.ParameterBuilder;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.essentials.Essentials;
import org.bukkit.entity.Player;

public class FlyCommand implements AunaCommand {
    @Override
    public Command createCommand() {
        return CommandBuilder.beginCommand("fly")
                .permission("citybuild.fly")
                .parameter(ParameterBuilder.beginParameter("Player")
                        .optional()
                        .autoCompletionHandler(new PlayerCompletionHandler())
                        .permission("citybuild.fly.other")
                        .build())
                .handler((aunaPlayer, context, strings) -> {
                    AunaPlayer t = aunaPlayer;
                    if (context.hasParameter("player")) {
                        t = AunaAPI.getApi().getPlayer(context.getParameterValue("player", Player.class).getUniqueId());
                    }
                    int toggleMode = Essentials.getInstance().getFlyManager().toggleFly(t.toBukkitPlayer());
                    if (t.getUuid().equals(aunaPlayer.toBukkitPlayer().getUniqueId())) {
                        aunaPlayer.sendMessage("citybuild.fly." + toggleMode);
                    } else {
                        aunaPlayer.sendMessage("citybuild.fly." + toggleMode + ".other", t.getName());
                        t.sendMessage("citybuild.fly." + toggleMode);
                    }
                })
                .build();
    }
}
