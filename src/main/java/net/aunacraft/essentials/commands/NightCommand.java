package net.aunacraft.essentials.commands;

import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.builder.CommandBuilder;

public class NightCommand implements AunaCommand {

    @Override
    public net.aunacraft.api.commands.Command createCommand() {
        return CommandBuilder.beginCommand("night")
                .permission("essentials.night")
                .handler((p, context, args) -> {
                    p.toBukkitPlayer().getWorld().setTime(14000);
                    p.sendMessage("essentials.night");
                }).build();
    }
}
