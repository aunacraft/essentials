package net.aunacraft.essentials.commands;

import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.builder.CommandBuilder;
import org.bukkit.World;

import java.util.Random;

public class RainCommand implements AunaCommand {

    @Override
    public net.aunacraft.api.commands.Command createCommand() {
        return CommandBuilder.beginCommand("rain")
                .permission("essentials.rain")
                .handler((p, context, args) -> {
                    int duration = (300 + new Random().nextInt(600)) * 20;
                    World world = p.toBukkitPlayer().getWorld();

                    world.setWeatherDuration(duration);
                    world.setThunderDuration(duration);

                    p.toBukkitPlayer().getWorld().setStorm(true);
                    p.toBukkitPlayer().getWorld().setThundering(false);

                    p.sendMessage("essentials.rain");
                }).build();
    }

}
