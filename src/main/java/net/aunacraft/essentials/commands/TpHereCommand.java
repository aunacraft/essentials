package net.aunacraft.essentials.commands;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.Command;
import net.aunacraft.api.commands.builder.CommandBuilder;
import net.aunacraft.api.commands.builder.ParameterBuilder;
import net.aunacraft.essentials.commands.autocompletion.ExtendedPlayerAutoCompletionHandler;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TpHereCommand implements AunaCommand {
    @Override
    public Command createCommand() {
        return CommandBuilder.beginCommand("tphere")
                .permission("citybuild.tphere")
                .parameter(ParameterBuilder.beginParameter("Player")
                        .required()
                        .autoCompletionHandler(new ExtendedPlayerAutoCompletionHandler())
                        .build())
                .handler((aunaPlayer, context, strings) -> {
                    List<Player> playersToTeleport = new ArrayList<>(Bukkit.getOnlinePlayers());
                    if (!context.getParameterValue("Player", String.class).equalsIgnoreCase("all")) {
                        playersToTeleport = Arrays.asList(Bukkit.getPlayer(context.getParameterValue("Player", String.class)));
                    }

                    for (Player p : playersToTeleport) {
                        p.teleport(aunaPlayer.toBukkitPlayer().getLocation());
                        AunaAPI.getApi().getPlayer(p.getUniqueId()).sendMessage("citybuild.tphere.teleportedto", aunaPlayer.getName());
                    }
                    aunaPlayer.sendMessage("citybuild.tphere.teleported");
                })
                .build();
    }
}
